// Package lscm provides abstractions of the Kubernetes client to list ConfigMaps in a cluster.
package lscm
