package main

import (
	"flag"
	"fmt"
	"os"
	"text/tabwriter"
	"time"

	"gitlab.com/wmedlar/lscm"
)

var (
	headers = "NAMESPACE\tNAME\tDATA\tAGE"
	version = "v0.0.0-dev"
)

// client-go uses glog for logging, which adds some flags we'd rather it not
// instead we'll use a FlagSet to whitelist only our defined flags
var flags = flag.NewFlagSet("lscm", flag.ExitOnError)

var (
	context          = flags.String("context", "", "Name of a kubeconfig context to use.")
	impersonateUser  = flags.String("user", "", "List ConfigMaps as this user.")
	impersonateGroup = flags.String("group", "", "List ConfigMaps as this group. Requires -user.")
	fromNamespace    = flags.String("namespace", "", "List ConfigMaps from this namespace.")

	allNamespaces = flags.Bool("all-namespaces", false, "List ConfigMaps from all namespaces.")
	showVersion   = flags.Bool("version", false, "Display version number and exit.")
)

func main() {
	flags.Parse(os.Args[1:])

	if *showVersion {
		fmt.Println(version)
		os.Exit(0)
	}

	if *impersonateUser == "" && *impersonateGroup != "" {
		fmt.Println("error: user cannot be empty if group is set")
		os.Exit(1)
	}

	config, err := lscm.GetContextConfig(*context)
	if err != nil {
		fmt.Println("error:", err)
		os.Exit(1)
	}

	config.Impersonate(*impersonateUser, *impersonateGroup)
	client, err := lscm.NewClient(config)

	var namespace string
	switch {
	case *allNamespaces:
		namespace = lscm.AllNamespaces
	case *fromNamespace != "":
		namespace = *fromNamespace
	default:
		// use context-defined or default namespace
		namespace = config.Namespace
	}

	configmaps, err := client.ListConfigMaps(namespace)
	if err != nil {
		fmt.Println("error:", err)
		os.Exit(1)
	}

	if len(configmaps) == 0 {
		fmt.Println("No resources found.")
		os.Exit(0)
	}

	w := new(tabwriter.Writer)
	w.Init(os.Stdout, 0, 8, 0, '\t', 0)
	fmt.Fprintln(w, headers)

	for _, cm := range configmaps {
		age := time.Since(cm.ObjectMeta.CreationTimestamp.Time)
		row := fmt.Sprintf("%s\t%s\t%d\t%dd", cm.ObjectMeta.Namespace, cm.ObjectMeta.Name, len(cm.Data), toDays(age))
		fmt.Fprintln(w, row)
	}

	w.Flush()
}

func toDays(d time.Duration) int {
	return int(d.Hours() / 24)
}
