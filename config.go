package lscm

import (
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

// Config encapsulates a single context's cluster configuration.
type Config struct {
	// Namespace is the context-defined namespace from a kubeconfig.
	// If no namespace is defined in the parsed config, this will
	// be the cluster default namespace ("default").
	Namespace string
	config    *rest.Config
}

// Impersonate modifies the configuration to make subsequent requests
// as a given user or group. User or group can be an empty string to
// leave the previous value unchanged.
func (c *Config) Impersonate(user, group string) {
	if user != "" {
		c.config.Impersonate.UserName = user
	}

	if group != "" {
		c.config.Impersonate.Groups = []string{group}
	}
}

// ClearImpersonation resets the impersonation config to its default values.
func (c *Config) ClearImpersonation() {
	c.config.Impersonate.UserName = ""
	c.config.Impersonate.Groups = make([]string, 0)
}

// RestConfig exposes the underlying rest.Config.
func (c *Config) RestConfig() *rest.Config {
	return c.config
}

// GetContextConfig finds a Kubernetes config file and returns the
// config and namespace of a specified context. Like kubectl, this
// will respect the KUBECONFIG environment variable. If not set it
// will search for ~/.kube/config or ~/.kube/.kubeconfig. If the
// config file does not exist or cannot be found, this will return
// an error.
func GetContextConfig(context string) (*Config, error) {
	loadingRules := clientcmd.NewDefaultClientConfigLoadingRules()
	configOverrides := &clientcmd.ConfigOverrides{
		CurrentContext: context,
	}
	kubeConfig := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(loadingRules, configOverrides)

	// strangely the context namespace isn't stored within the rest client config
	namespace, _, err := kubeConfig.Namespace()
	if err != nil {
		return nil, err
	}

	config, err := kubeConfig.ClientConfig()
	if err != nil {
		return nil, err
	}

	return &Config{namespace, config}, nil
}
