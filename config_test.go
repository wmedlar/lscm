package lscm

import (
	"testing"

	"k8s.io/client-go/tools/clientcmd"
)

var impersonationTest = []struct {
	user  string
	group string
}{
	{"", ""},
	{"user", ""},
	{"", "group"},
	{"user", "group"},
}

func TestConfigImpersonation(t *testing.T) {
	defaultConfig, err := clientcmd.BuildConfigFromFlags("", "test_kubeconfig.yaml")
	if err != nil {
		t.Fatal(err)
	}

	var config *Config

	for _, v := range impersonationTest {
		config = &Config{"", defaultConfig}
		config.Impersonate(v.user, v.group)

		impersonation := config.RestConfig().Impersonate
		if impersonation.UserName != v.user {
			t.Errorf("Config.Impersonate(%#v, %#v): expected user %#v, got %#v", v.user, v.group, v.user, impersonation.UserName)
		}

		switch v.group {
		case "":
			if len(impersonation.Groups) != 0 {
				t.Errorf("Config.Impersonate(%#v, %#v): expected groups [], got %#v", v.user, v.group, impersonation.Groups)
			}
		default:
			// Impersonate.Group is only set if specified group is non-empty
			if len(impersonation.Groups) != 1 || impersonation.Groups[0] != v.group {
				t.Errorf("Config.Impersonate(%#v, %#v): expected groups [ %#v ], got %#v", v.user, v.group, v.group, impersonation.Groups)
			}
		}

		config.ClearImpersonation()

		resetImpersonation := config.RestConfig().Impersonate
		if resetImpersonation.UserName != "" || len(resetImpersonation.Groups) != 0 {
			t.Errorf("Config.ClearImpersonation(): expected user \"\" and group [], got %#v, %#v", resetImpersonation.UserName, resetImpersonation.Groups)
		}
	}
}
