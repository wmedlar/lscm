# lscm
lscm lists the ConfigMaps accessible in a Kubernetes cluster.

## Installation
lscm is `go get`-able for users with existing Golang development environments.

```shell
$ go get -u gitlab.com/wmedlar/lscm/cmd/lscm
```

Gitlab doesn't have the binary release support like Github does quite yet, so this is the only method of installation currently available.

## Usage
```shell
$ lscm -h
Usage of lscm:
  -all-namespaces
    	List ConfigMaps from all namespaces.
  -context string
    	Name of a kubeconfig context to use.
  -group string
    	List ConfigMaps as this group. Requires -user.
  -namespace string
    	List ConfigMaps from this namespace.
  -user string
    	List ConfigMaps as this user.
  -version
    	Display version number and exit.
$ lscm -user wmedlar -all-namespaces
NAMESPACE         NAME                                   DATA     AGE
ingress-nginx     ingress-controller-leader-nginx        0        13d
ingress-nginx     nginx-configuration                    1        13d
ingress-nginx     tcp-services                           0        13d
ingress-nginx     udp-services                           0        13d
kube-system       calico-config                          3        34d
kube-system       extension-apiserver-authentication     1        34d
kube-system       kube-dns-autoscaler                    1        34d
```

lscm makes use of your Kubernetes config found at either `~/.kube/config`, `~/.kube/.kubeconfig`, or from a path defined by the `KUBECONFIG` environment variable.

## Caveats
- When using the `-all-namespaces` flag, the user making the request must have the ability to list ConfigMaps at the cluster level. The minimum necessary permissions for this can be found in [clusterrole.yaml](clusterrole.yaml) (note: it _must_ be a ClusterRole, not a Role, to be able to list resources at the cluster level). Bind this role to a user with kubectl:

```shell
$ kubectl create -f https://gitlab.com/wmedlar/lscm/raw/master/clusterrole.yaml
$ kubectl create clusterrolebinding --clusterrole list-configmaps --user wmedlar wmedlar-view-all
```

- Groups cannot be impersonated without specifying a user. This is a requirement of the Kubernetes API server.

- lscm segfaults when trying to authenticate with Google Cloud clusters. There is custom handling in the Kubernetes Go client for GCP clusters, however this feature hasn't been implemented in lscm yet.

## Development
Clone the repo, then install project dependencies with [dep](https://github.com/golang/dep).

```shell
$ dep ensure
```

Run `go test -short` to run unit tests, and `go test` to run the full test suite, including integration tests. This will require you to have a valid kubeconfig and the ability to create/list/delete ConfigMaps in your context's namespace. Unit tests are currently run in CI, integration tests are run locally. 

lscm uses [golint](https://github.com/golang/lint) and [goimports](https://godoc.org/golang.org/x/tools/cmd/goimports) for linting and styling, respectively. These, as well, are run in CI.
