package lscm

import (
	"testing"

	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var testConfigMap = &v1.ConfigMap{
	Data: map[string]string{"unban": "twin"},
	ObjectMeta: metav1.ObjectMeta{
		GenerateName: "lscm-test-configmap-",
	},
}

func TestClientIntegration(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration tests in short mode")
	}

	config, err := GetContextConfig("")
	if err != nil {
		t.Fatal(err)
	}

	client, err := NewClient(config)
	if err != nil {
		t.Fatal(err)
	}

	expected, err := client.Clientset().CoreV1().ConfigMaps(config.Namespace).Create(testConfigMap)
	if err != nil {
		t.Fatal("creating configmap failed:", err)
	}
	defer client.Clientset().CoreV1().ConfigMaps(config.Namespace).Delete(expected.Name, &metav1.DeleteOptions{})

	actual, err := client.ListConfigMaps(config.Namespace)
	if err != nil {
		t.Fatal("error retrieving configmaps:", err)
	}

	var exists bool
	for _, configmap := range actual {
		if configmap.ObjectMeta.Namespace == expected.ObjectMeta.Namespace && configmap.ObjectMeta.Name == expected.ObjectMeta.Name {
			exists = true
			break
		}
	}

	if !exists {
		t.Errorf("Client.ListConfigMaps(%#v): expected value %#v not found in %#v", config.Namespace, expected, actual)
	}
}
