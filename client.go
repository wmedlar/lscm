package lscm

import (
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

// AllNamespaces is an empty string signifying no namespace filtering.
const AllNamespaces = ""

// Client is an abstraction of the Kubernetes clientset to simplify
// listing ConfigMaps in a cluster.
type Client struct {
	client *kubernetes.Clientset
}

// NewClient creates a Client from a cluster configuration.
func NewClient(config *Config) (*Client, error) {
	clientset, err := kubernetes.NewForConfig(config.RestConfig())
	if err != nil {
		return nil, err
	}

	return &Client{clientset}, nil
}

// Clientset exposes the underlying kubernetes.Clientset.
func (c *Client) Clientset() *kubernetes.Clientset {
	return c.client
}

// ListConfigMaps retrieves the ConfigMaps in a given namespace.
// If namespace is an empty string then ConfigMaps in all namespaces
// will be returned.
func (c *Client) ListConfigMaps(namespace string) ([]v1.ConfigMap, error) {
	list, err := c.client.CoreV1().ConfigMaps(namespace).List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	return list.Items, nil
}
